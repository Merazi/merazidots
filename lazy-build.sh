#!/bin/bash
clear;

# check for root access
if [ "$EUID" -ne 0 ]
then
        echo "Please run the script as root";
        exit;
fi

# display current task with figlet
figlet "Lazy build";
sleep 1;

# get the current directory, the script must be run in the root of the repo
dir=$(pwd);

# this are the build steps, merge the config files and then compile
function s_build(){
        figlet "$1";
        make install clean;
}

# this is why the script must be run from the root of the repo:
clear;
figlet "Building...";
cd $dir/dwm/      && s_build "dwm";
cd $dir/dmenu/    && s_build "dmenu";
cd $dir/st/       && s_build "st";
cd $dir/slstatus/ && s_build "slstatus";
