# Merazi's dotfiles
This repository includes:

- My fork of [dwm](https://dwm.suckless.org)
- My fork of [dmenu](https://tools.suckless.org/dmenu)
- My fork of [st](https://st.suckless.org)
- My fork of [slstatus](https://tools.suckless.org/slstatus)
- A script used for building everything

All the added patches are stored in the "patches" you can use "lazy-build" to build everything automatically, just check you have all the necessary dependencies. Figlet is not required for the script to work, it is only used to display some messages. The script must be invoked as root.

## How it looks
![screenshot](screenshot.png)

## Things you'll need
- make
- gcc compiler
- xlib headers

## I'm using this fonts:
- xos4 Terminus font
- CodeNewRoman nerd font
If you want another fonts, you can add them in the config.h of each program

## Installation
If you want to install everything, then do:

        # ./lazy-build.sh

This will compile dwm, dmenu and st. You can compile each one of them manually, entering each one of the directories and running:

        # make install

or:

        # make clean install

## TODO
- update the screenshot
- organize the code a little more, I'm changing the code formatting
