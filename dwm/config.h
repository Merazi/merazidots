#include<X11/XF86keysym.h>      // for media keys support

/* appearance */
#define GAPS_VALUE 8
static const unsigned int gappx          = GAPS_VALUE;
static const unsigned int borderpx       = 0;   /* border pixel of windows */
static const unsigned int snap           = 32;  /* snap pixel */
static const unsigned int systraypinning = 0;   /* 0: sloppy systray follows selected monitor, >0: pin systray to monitor X */
static const unsigned int systrayspacing = 2;   /* systray spacing */
static const int systraypinningfailfirst = 1;   /* 1: if pinning fails, display systray on the first monitor, else last monitor*/
static const int showsystray             = 1;   /* 0 means no systray */
static const int showbar                 = 1;   /* 0 means no bar */
static const int topbar                  = 1;   /* 0 means bottom bar */
static const char col_gray1[]            = "#20201d"; // tags and tray bg color
static const char col_gray2[]            = "#20201d"; // borders (I don't use them)
static const char col_gray3[]            = "#a6a28c"; // unfocused tags and tray fg colors
static const char col_gray4[]            = "#20201d"; // focused tag and title fg colors
static const char col_cyan[]             = "#6684e1"; // focused tag and title bg colors

/* fonts */
static const char *fonts[]               = { 
        "Source Code Pro:style=Book:size=10",
        "Sazanami Mincho,さざなみ明朝:size=8",
        "CodeNewRoman Nerd Font:style=Book:size=10",
        "Baekmuk Gulim,백묵 굴림:size=10",
};

/* color assignment */
static const char *colors[][3]           = {
        /*               fg         bg         border   */
        [SchemeNorm] = { col_gray3, col_gray1, col_gray2 },
        [SchemeSel]  = { col_gray4, col_cyan,  col_cyan  },
};

/* no actual rules */
static Rule rules[] = {
        /* class      instance    title       tags mask     isfloating   monitor */
        { NULL,       NULL,       NULL,       0,            False,       -1 },
};

/* tags in japanese cuz i'm a weeb */
static const char *tags[] = { "一", "二", "三", "四", "五", "六", "七", "八", "九" };

/* layout(s) */
static const float mfact      = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster      = 1;    /* number of clients in master area */
static const int resizehints  = 0;    /* 1 means respect size hints in tiled resizals */
static const Layout layouts[] = {
        /* symbol     arrange function */
        { "[]=",      tile },    /* first entry is default */
        { "><>",      NULL },    /* no layout function means floating behavior */
        { "[M]",      monocle },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "3"; /* component of dmenucmd, manipulated in spawn() */

/* application commands (you might want to change some of these) */
static const char *dmenucmd[]   = { "dmenu_run", NULL };
static const char *termcmd[]    = { "st", NULL };
static const char *browsercmd[] = { "firefox", NULL };
static const char *gfilescmd[]  = { "thunar", NULL };
static const char *filescmd[]   = { "st", "ranger", NULL };

/* volume control commands (with pulseaudio) */
static const char *upvol[]   = { "/usr/bin/pactl", "set-sink-volume", "0", "+10%", NULL };
static const char *downvol[] = { "/usr/bin/pactl", "set-sink-volume", "0", "-10%", NULL };
static const char *mutevol[] = { "/usr/bin/pactl", "set-sink-mute",   "0", "toggle", NULL };

static Key keys[] = {
        /* modifier         key        function        argument */
        /* application launching */
        { MODKEY,           XK_d,      spawn,          {.v = dmenucmd  }},
        { MODKEY|ShiftMask, XK_Return, spawn,          {.v = termcmd   }},
        { MODKEY,           XK_w,      spawn,          {.v = browsercmd}},
        { MODKEY,           XK_f,      spawn,          {.v = filescmd}},
        { MODKEY|ShiftMask, XK_f,      spawn,          {.v = gfilescmd}},

        /* window managing */
        { MODKEY,           XK_b,      togglebar,      {0} },
        { MODKEY,           XK_j,      focusstack,     {.i = +1   }},
        { MODKEY,           XK_k,      focusstack,     {.i = -1   }},
        { MODKEY|ShiftMask, XK_i,      incnmaster,     {.i = +1   }},
        { MODKEY|ShiftMask, XK_d,      incnmaster,     {.i = -1   }},
        { MODKEY,           XK_h,      setmfact,       {.f = -0.05}},
        { MODKEY,           XK_l,      setmfact,       {.f = +0.05}},
        { MODKEY,           XK_Return, zoom,           {0}},
        { MODKEY,           XK_Tab,    view,           {0}},
        { MODKEY|ShiftMask, XK_c,      killclient,     {0}},

        /* layouts */
        { MODKEY,           XK_i,      setlayout,      {.v = &layouts[0]}},
        { MODKEY,           XK_o,      setlayout,      {.v = &layouts[1]}},
        { MODKEY,           XK_p,      setlayout,      {.v = &layouts[2]}},
        { MODKEY,           XK_space,  setlayout,      {0}},
        { MODKEY|ShiftMask, XK_space,  togglefloating, {0}},
        { MODKEY,           XK_0,      view,           {.ui = ~0 }},
        { MODKEY|ShiftMask, XK_0,      tag,            {.ui = ~0 }},
        { MODKEY,           XK_comma,  focusmon,       {.i = -1  }},
        { MODKEY,           XK_period, focusmon,       {.i = +1  }},
        { MODKEY|ShiftMask, XK_comma,  tagmon,         {.i = -1  }},
        { MODKEY|ShiftMask, XK_period, tagmon,         {.i = +1  }},

        /* gaps management */
        { MODKEY,           XK_minus, setgaps, {.i = -1}},
        { MODKEY,           XK_equal, setgaps, {.i = +1}},
        { MODKEY,           XK_g,     setgaps, {.i = 0 }},
        { MODKEY|ShiftMask, XK_g,     setgaps, {.i = GAPS_VALUE}},

        /* tag navigation */
        TAGKEYS(                    XK_1,                      0)
                TAGKEYS(            XK_2,                      1)
                TAGKEYS(            XK_3,                      2)
                TAGKEYS(            XK_4,                      3)
                TAGKEYS(            XK_5,                      4)
                TAGKEYS(            XK_6,                      5)
                TAGKEYS(            XK_7,                      6)
                TAGKEYS(            XK_8,                      7)
                TAGKEYS(            XK_9,                      8)

                /* exit dwm */
                { MODKEY|ShiftMask, XK_q,      quit,           {0} },

        /* volume control with and without media keys */
                { 0,                XF86XK_AudioLowerVolume, spawn,  {.v = downvol } },
                { 0,                XF86XK_AudioMute,        spawn,  {.v = mutevol } },
                { 0,                XF86XK_AudioRaiseVolume, spawn,  {.v = upvol   } },
                { MODKEY|ShiftMask, XK_v,                    spawn,  {.v = downvol } },
                { MODKEY,           XK_m,                    spawn,  {.v = mutevol } },
                { MODKEY,           XK_v,                    spawn,  {.v = upvol   } },
};

/* buttons definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
        /* click         ev mask    button   function        argument */
        { ClkLtSymbol,   0,         Button1, setlayout,      {0} },
        { ClkLtSymbol,   0,         Button3, setlayout,      {.v = &layouts[2]} },
        { ClkWinTitle,   0,         Button2, zoom,           {0} },
        { ClkStatusText, 0,         Button2, spawn,          {.v = termcmd } },
        { ClkClientWin,  MODKEY,    Button1, movemouse,      {0} },
        { ClkClientWin,  MODKEY,    Button2, togglefloating, {0} },
        { ClkClientWin,  MODKEY,    Button3, resizemouse,    {0} },
        { ClkTagBar,     0,         Button1, view,           {0} },
        { ClkTagBar,     0,         Button3, toggleview,     {0} },
        { ClkTagBar,     MODKEY,    Button1, tag,            {0} },
        { ClkTagBar,     MODKEY,    Button3, toggletag,      {0} },
};

